registerSettingsPage(settingsComponent);

export type SettingsKeysType = keyof SettingsType;
export type SettingsType = {
    highValue: number,
    medValue: number,
    lowValue: number,
    starting: number,
    interval: number,
    defaultRed: boolean,
    synched: boolean,
};

function setDefaults(props: SettingsComponentProps) {
    const defaultSettings = {
        highValue: '100',
        medValue: '50',
        lowValue: '0',
        starting: JSON.stringify({ values: [{ name: 'high', value: '2' }], selected: [0] }),
        interval: JSON.stringify({ name: '30' }),
        defaultRed: 'false',
        synched: 'false',
    };

    for (const key in defaultSettings) {
        if (!props.settings[key]) {
            props.settingsStorage.setItem(key, defaultSettings[key as SettingsKeysType]);
        }
    }
}

function settingsComponent(props: SettingsComponentProps) {
    setDefaults(props);

    return (
        <Page>
            <Section title={ <Text bold align='center'>Flashy Settings</Text> }>
                <Slider settingsKey='highValue' max='100' min='0'
                    label={`high brightness value (${props.settings.highValue}%)`}
                />
                <Slider settingsKey='medValue' max='100' min='0'
                    label={`medium brightness value (${props.settings.medValue}%)`}
                />
                <Slider settingsKey='lowValue' max='100' min='0'
                    label={`low brightness value (${props.settings.lowValue}%)`}
                />
                <TextInput type='number' label='vibration interval (seconds)' settingsKey='interval' />
                <Select label='starting brightness' settingsKey='starting'
                    options={[
                        { name:'high', value:'2' },
                        { name:'medium', value:'1' },
                        { name:'low', value:'0' },
                    ]}
                    renderItem={(option) => { return <Text>{option.name}</Text>; }}
                />
                <Toggle label='start with red light' settingsKey='defaultRed' />
            </Section>
        </Page>
    );
}
