import { encode } from 'cbor';
import { outbox } from 'file-transfer';
import { app } from 'peer';
import { settingsStorage } from 'settings';
import { SettingsType } from '../settings';
import { SettingsRepository } from './SettingsRepository';

export class SettingsListener {
    private settings: SettingsType;
    private outboxTimeout: number;

    constructor () {
        this.settings = this.getAllSettings();
    }

    public subscribe() {
        settingsStorage.addEventListener('change', this.settingsChangeHandler.bind(this));
    }

    public async sendSettingsToWatch() {
        if (app.readyState !== 'started') {
            return;
        }

        try {
            await outbox.enqueue('settings.cbor', encode(this.settings));
            SettingsRepository.setToggleSetting('synched', true);
            console.log(`settings sent: ${JSON.stringify(this.settings)}`);
        } catch (ex) {
            console.log(`Error sending settings: ${JSON.stringify(ex)}`);
        }
    }

    private getAllSettings(): SettingsType {
        let interval = parseInt(SettingsRepository.getTextInputSetting('interval'), 10);
        if (interval === NaN || interval < 1) {
            interval = 1;
            SettingsRepository.setTextInputSetting('interval', '1');
        }

        return {
            highValue: SettingsRepository.getSliderSetting('highValue', 100) / 100.0,
            medValue: SettingsRepository.getSliderSetting('medValue', 50) / 100.0,
            lowValue: SettingsRepository.getSliderSetting('lowValue', 0) / 100.0,
            starting: parseInt(SettingsRepository.getSelectSingleSetting('starting', '2'), 10),
            interval: interval * 1000,
            defaultRed: SettingsRepository.getToggleSetting('defaultRed', false),
            synched: SettingsRepository.getToggleSetting('synched', false),
        };
    }

    private settingsChangeHandler() {
        SettingsRepository.setToggleSetting('synched', false);

        this.settings = this.getAllSettings();

        clearTimeout(this.outboxTimeout);
        this.outboxTimeout = setTimeout(this.sendSettingsToWatch.bind(this), 500);
    }
}
