import { display } from 'display';
import document from 'document';
import { readFileSync } from 'fs';
import { VibrationHandler } from './VibrationHandler';

export class SettingsHandler {
    private brightnessValues: number[];
    private currentBrightness: number;
    private defaultRed: boolean;
    private vibrationInterval: number;

    constructor (private readonly vibrationHandler: VibrationHandler) {}

    public getCurrentBrightness(): number {
        return this.currentBrightness;
    }

    public increaseBrightness(): void {
        if (this.currentBrightness === this.brightnessValues.length - 1) {
            return;
        }

        console.log(`brightness to ${this.currentBrightness}`);

        this.currentBrightness += 1;
        display.brightnessOverride = this.brightnessValues[this.currentBrightness];
    }

    public decreaseBrightness(): void {
        if (this.currentBrightness === 0) {
            return;
        }

        console.log(`brightness to ${this.currentBrightness}`);

        this.currentBrightness -= 1;
        display.brightnessOverride = this.brightnessValues[this.currentBrightness];
    }

    public updateSettings(): void {
        this.readSettings();

        display.brightnessOverride = this.brightnessValues[this.currentBrightness];
        display.autoOff = false;

        this.setColors(this.defaultRed);

        this.vibrationHandler.setTime(this.vibrationInterval);
        this.vibrationHandler.restart();
    }

    private readSettings(): void {
        let settings;

        try {
            settings = readFileSync('settings.cbor', 'cbor');
            console.log(`found settings: ${JSON.stringify(settings)}`);
        } catch (ex) {
            settings = {
                highValue: 1.0,
                medValue: 0.5,
                lowValue: 0.0,
                interval: 30000,
                starting: 2,
                defaultRed: false,
            };
            console.log('error reading settings, using defaults');
        }

        this.brightnessValues = [
            settings.lowValue,
            settings.medValue,
            settings.highValue,
        ];

        this.currentBrightness = settings.starting;
        this.vibrationInterval = settings.interval;
        this.defaultRed = settings.defaultRed;

        if (isNaN(this.vibrationInterval)) {
            this.vibrationInterval = 30000;
        }

        return;
    }

    private setColors(defaultRed: boolean): void {
        const defaultPage = document.getElementById('default') as GraphicsElement;
        const secondaryPage = document.getElementById('secondary') as GraphicsElement;

        if (defaultRed === true) {
            defaultPage.style.fill = 'red';
            secondaryPage.style.fill = 'white';
            return;
        }

        defaultPage.style.fill = 'white';
        secondaryPage.style.fill = 'red';
    }
}
