import { inbox } from 'file-transfer';
import { SettingsHandler } from './SettingsHandler';

export class InboxListener {
    constructor (private readonly settingsHandler: SettingsHandler) {}

    public subscribe() {
        inbox.addEventListener('newfile', this.inboxHandler.bind(this));
    }

    private inboxHandler() {
        let fileName;

        while (fileName = inbox.nextFile()) {
            console.log(`File received: ${fileName}`);

            if (fileName === 'settings.cbor') {
                this.settingsHandler.updateSettings();
            }
        }
    }
}
