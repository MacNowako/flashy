There are quite a few flashlight and torch apps on the gallery, but none of them exactly matched the features that I wanted so I wrote my own. Maybe this will have the feature-set someone else wants too.

----------

Features:

    - screen always on
    - three levels of brightness
    - side buttons adjust brightness up and down
    - cover screen to quit app (so you don't accidentally turn it back on again)
    - periodic vibration to remind you that the light is running
    - swipe between red and white light

----------

Settings:

    - adjust individual brightness levels for low/med/high
    - adjust seconds between vibration reminders
    - app starts on low/medium/high brightness by default
    - app starts on red or white light by default